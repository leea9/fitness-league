# General Information
---

This application allows easy workout tracking by allowing users to scan QR codes to track what machine they are using.

This application will be used mainly in the Mueller center at RPI for now.

