package com.sdd.fitnessleague.test;

import android.content.Intent;
import android.test.ActivityInstrumentationTestCase2;

import com.sdd.fitnessleague.ExerciseActivity;
import android.widget.EditText;
import java.util.ArrayList;

public class ExerciseActivityTest extends
		ActivityInstrumentationTestCase2<ExerciseActivity> {
	public final static String EXERCISE_SELECTED = "com.sdd.fitnessleague.MESSAGE";

	private ExerciseActivity mActivity;
	private ArrayList<EditText> mEditTextList;
	
	public ExerciseActivityTest(){
		super(ExerciseActivity.class);
	}
	
	protected void setUp() throws Exception{
		super.setUp();
		setActivityInitialTouchMode(false);
		mEditTextList = new ArrayList<EditText>();

		Intent i = new Intent();
		i.setClassName("com.sdd.fitnessleague", "com.sdd.fitnessleague.ExerciseActivity");
        i.putExtra(EXERCISE_SELECTED, "Unknown");
        setActivityIntent(i);
        
		mActivity = getActivity();		
		mEditTextList.add((EditText) mActivity.findViewById(com.sdd.fitnessleague.R.id.weight1));
		mEditTextList.add((EditText) mActivity.findViewById(com.sdd.fitnessleague.R.id.weight2));
		mEditTextList.add((EditText) mActivity.findViewById(com.sdd.fitnessleague.R.id.weight3));
		mEditTextList.add((EditText) mActivity.findViewById(com.sdd.fitnessleague.R.id.reps1));
		mEditTextList.add((EditText) mActivity.findViewById(com.sdd.fitnessleague.R.id.reps2));
		mEditTextList.add((EditText) mActivity.findViewById(com.sdd.fitnessleague.R.id.reps3));
	}
	
	public void testPreConditions() {
		assertNotNull(mEditTextList);
		for (int i = 0; i < mEditTextList.size(); i++){
			assertNotNull(mEditTextList.get(i));
		}
	}
	
	public void testText() {
		assertNotNull(mEditTextList);
		for (int i = 0; i < mEditTextList.size(); i++){
			assertEquals(0, mEditTextList.get(i).getText().length());
		}
	}
	
}
