package com.sdd.fitnessleague.test;

import android.test.ActivityInstrumentationTestCase2;
import com.sdd.fitnessleague.AddExercise;
import android.widget.EditText;

public class AddExerciseTest extends
		ActivityInstrumentationTestCase2<AddExercise> {

	private AddExercise mActivity;
	private EditText mEditTitle;
	private EditText mEditDesc;
	
	public AddExerciseTest(){
		super(AddExercise.class);
	}
	
	protected void setUp() throws Exception{
		super.setUp();
		setActivityInitialTouchMode(false);
		mActivity = getActivity();
		mEditTitle = (EditText) mActivity.findViewById(com.sdd.fitnessleague.R.id.add_ex_title);
		mEditDesc = (EditText) mActivity.findViewById(com.sdd.fitnessleague.R.id.add_ex_desc);
	}
	
	public void testPreConditions() {
		assertNotNull(mEditTitle);
		assertNotNull(mEditDesc);
	}
	
	public void testText() {
		assertEquals(0, mEditTitle.getText().length());
		assertEquals(0, mEditDesc.getText().length());
	}
	
}
