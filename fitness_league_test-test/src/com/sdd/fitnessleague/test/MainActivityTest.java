package com.sdd.fitnessleague.test;

import android.app.Activity;
import android.app.Instrumentation.ActivityMonitor;
import android.test.ActivityInstrumentationTestCase2;
import com.sdd.fitnessleague.MainActivity;
import com.sdd.fitnessleague.ExerciseListActivity;
import com.dm.zbar.android.scanner.ZBarScannerActivity;
import com.sdd.fitnessleague.LogActivity;
import android.widget.Button;

public class MainActivityTest extends
			ActivityInstrumentationTestCase2<MainActivity> {
	
	private MainActivity mActivity;
	
	public MainActivityTest(){
		super(MainActivity.class);
	}
	
	protected void setUp() throws Exception{
		super.setUp();
		setActivityInitialTouchMode(false);
		mActivity = getActivity();
	}
	
	public void testPreConditions() {
		assertNotNull(mActivity);
	}
	
	public void testOpenExerciseList() {
		// register next activity that need to be monitored.
		  ActivityMonitor activityMonitor = getInstrumentation().addMonitor(ExerciseListActivity.class.getName(), null, false);
		  
		  final Button button = (Button) mActivity.findViewById(com.sdd.fitnessleague.R.id.button1);
		  mActivity.runOnUiThread(new Runnable() {
		    @Override
		    public void run() {
		      // click button and open next activity.
		      button.performClick();
		    }
		  });
		  
		  Activity nextActivity = getInstrumentation().waitForMonitorWithTimeout(activityMonitor, 5000);
		  // next activity is opened and captured.
		  assertNotNull(nextActivity);
		  nextActivity.finish();
		  setActivity(null);
	}
	
	public void testOpenQrScan() {
		// register next activity that need to be monitored.
		  ActivityMonitor activityMonitor = getInstrumentation().addMonitor(ZBarScannerActivity.class.getName(), null, false);
		  
		  final Button button = (Button) mActivity.findViewById(com.sdd.fitnessleague.R.id.button3);
		  mActivity.runOnUiThread(new Runnable() {
		    @Override
		    public void run() {
		      // click button and open next activity.
		      button.performClick();
		    }
		  });
		  
		  Activity nextActivity = getInstrumentation().waitForMonitorWithTimeout(activityMonitor, 5000);
		  // next activity is opened and captured.
		  assertNotNull(nextActivity);
		  nextActivity.finish();
	}
	
	public void testOpenShowLog() {
		// register next activity that need to be monitored.
		  ActivityMonitor activityMonitor = getInstrumentation().addMonitor(LogActivity.class.getName(), null, false);
		  
		  final Button button = (Button) mActivity.findViewById(com.sdd.fitnessleague.R.id.log_button);
		  mActivity.runOnUiThread(new Runnable() {
		    @Override
		    public void run() {
		      // click button and open next activity.
		      button.performClick();
		    }
		  });
		  
		  Activity nextActivity = getInstrumentation().waitForMonitorWithTimeout(activityMonitor, 5000);
		  // next activity is opened and captured.
		  assertNotNull(nextActivity);
		  nextActivity.finish();
	}

}
