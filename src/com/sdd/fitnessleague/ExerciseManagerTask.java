package com.sdd.fitnessleague;

import android.os.AsyncTask;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * This class allows for the execution of network communication to the database
 * for exercise tasks in a separate thread.
 * @author willis4
 *
 */
public class ExerciseManagerTask extends AsyncTask<String, Void, ArrayList<String>> {
	    
	/**
	 * Function that will be performed in a separate task
	 * @return requestedStrings ArrayList<String> represents the data being collected
	 *  or null if adding an exercise or of the exercise is not found.
	 */
	    protected ArrayList<String> doInBackground(String... params) {
	        ArrayList<String> requestedStrings = new ArrayList<String>();
        	try {
        		// Keyword to add a new exercise to the list
        		if (params[0].equals("AddExercise"))
        		{
        			ExerciseManager.AddExercise(params[1], params[2]);
        		}
        		// Keyword to fetch all of the names of the exercises
        		else if (params[0].equals("listAll"))
	        	{
	        		JSONArray jsonList = ExerciseManager.GetAllExercises();
	        		for (int i = 0; jsonList != null && i < jsonList.length(); i++) {
	        			JSONObject jsonObj = jsonList.getJSONObject(i);
	        			requestedStrings.add(jsonObj.getString("name"));
	        		}
	        	}
        		else if (params[0].equals("listAllId"))
	        	{
	        		JSONArray jsonList = ExerciseManager.GetAllExercises();
	        		for (int i = 0; jsonList != null && i < jsonList.length(); i++) {
	        			JSONObject jsonObj = jsonList.getJSONObject(i);
	        			requestedStrings.add(jsonObj.getString("id"));
	        		}
	        	}
        		// Find the exercise that has the same name and return all details
	        	else
	        	{
	        		JSONArray jsonList = ExerciseManager.GetExerciseByName(params[0]);
	        		for (int i = 0; jsonList != null && i < jsonList.length(); i++) {
	        			JSONObject jsonObj = jsonList.getJSONObject(i);
        				if (jsonObj != null && jsonObj.has("name")){
        					requestedStrings.add(jsonObj.getString("name"));
	        				if (jsonObj.has("description")){
	        					requestedStrings.add(jsonObj.getString("description"));
	        				}
	        				else {
	        					requestedStrings.add("");
	        				}
	        				if (jsonObj.has("id")){
	        					requestedStrings.add(jsonObj.getString("id"));
	        				}
        				}
	        		}
	        	}
	        	
	            return requestedStrings;
	        } catch (Exception e) {
	        	e.printStackTrace();
	            return null;
	        }
	    }
}
