package com.sdd.fitnessleague;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.concurrent.ExecutionException;

/**
 * A class to represent all current exercises that are available
 * @author willis4
 *
 */
public class ExerciseListActivity extends BaseActivity {

	/**
	 * Method called upon creation, loads the current list of exercises in the database
	 */
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_exercise_list);
		setupActionBar();
		
		final ListView listview = (ListView) findViewById(R.id.exercise_list_view);
	    final ArrayList<String>list = new ArrayList<String>();
		
		try{
			// Execute command to fetch list of all exercises
	    	ExerciseManagerTask emt = new ExerciseManagerTask();
	    	ArrayList<String> result = emt.execute("listAll").get();
	    	if (result == null){
	    		list.add ("No exercises returned.");
	    	}
	    	else{
	    		// Add all to the current view
    			list.addAll(result);
		    }
	    }catch (ExecutionException e){
        	e.printStackTrace();
		}catch (InterruptedException e){
        	e.printStackTrace();
		}
	   
	    ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, list);
	    listview.setAdapter(adapter);  
	    
	    // Read the data when an item is selected
	    listview.setOnItemClickListener(new AdapterView.OnItemClickListener() 
	    {
	     public void onItemClick(AdapterView<?> parentView, View childView, int position, long id) 
	     {
	    	 // Start the Activity that corresponds to the selected item from the list
             startActivity(list.get(position));
	     }
	    });
	    
	  }
	
	/**
	 * Overridden to show the title 
	 */
	protected void setupActionBar() {
		super.setupActionBar();
		getActionBar().setDisplayShowTitleEnabled(true);
	}
	
	/**
	 * Starts the ExerciseActivity with the parameter of what exercise to start
	 * @param activityName The name of the exercise to be started
	 */
	protected void startActivity(String activityName)
	{
   		Intent intent = new Intent(this, ExerciseActivity.class);
        intent.putExtra(MainActivity.EXERCISE_SELECTED, activityName);
        startActivity(intent);
	}


	/**
	 * Applies unique menu to the ExerciseListActivity page
	 */
	public boolean onCreateOptionsMenu(Menu menu) {
		super.onCreateOptionsMenu(menu);
		getMenuInflater().inflate(R.menu.exercise_list, menu);
		return true;
	}
	
	/**
	 * Implemented to support unique exercise menu
	 */
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case R.id.add_exercise_menu:	
			// Start the activity to add a new exercise
			Intent intent = new Intent(this, AddExercise.class);
            startActivity(intent);
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
}