package com.sdd.fitnessleague;

import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.view.Menu;
import android.view.MenuItem;
import android.support.v4.app.NavUtils;

import com.facebook.Request;
import com.facebook.Response;
import com.facebook.Session;
import com.facebook.model.GraphUser;

/**
 * This class provides a baseline for all other activities in order to have a consistent interface
 */
public class BaseActivity extends Activity {
	static String uId = "";

	/**
	 * This class sets the initial layout. This is overridden by the child activities
	 */
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_base);
		// Show the Up button in the action bar.
		setupActionBar();
	}

	/**
	 * Sets the default configuration of the Action Bar
	 */
	protected void setupActionBar() {
		getActionBar().setDisplayHomeAsUpEnabled(true);
		getActionBar().setDisplayShowTitleEnabled(false);
		
		getActionBar().setDisplayShowCustomEnabled(true);
	}

	/**
	 * Adds the menu options that are specific to the base activity
	 * These menu options will be available for each activity that extends this class
	 */
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.base, menu);
		return true;
	}

	@Override
	/**
	 * Sets the functionality when the home menu and logout menu button are selected
	 */
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case android.R.id.home:
			// This ID represents the Home or Up button. In the case of this
			// activity, the Up button is shown. Use NavUtils to allow users
			// to navigate up one level in the application structure. For
			// more details, see the Navigation pattern on Android Design:
			//
			// http://developer.android.com/design/patterns/navigation.html#up-vs-back
			//

			NavUtils.navigateUpFromSameTask(this);
			return true;
			
		case R.id.logout:
	        Session session = Session.getActiveSession();
	        if (session != null) {
	            session.closeAndClearTokenInformation();
	            Intent in = new Intent(BaseActivity.this, LoginActivity.class);
	            startActivity(in);
	            super.finish();

	        }
	        return true;
	        
		case R.id.profile:
			Intent in = new Intent(this, UserProfileActivity.class);
			startActivity(in);
	        return true;
		}
		return super.onOptionsItemSelected(item);
	}
	
	/**
	 * Collects the user id from the current facebook session
	 */
	protected void setUID() {
		Session session = Session.getActiveSession();
		if (session != null && session.isOpened()) {
			Request.newMeRequest(session, new Request.GraphUserCallback() {
				@Override
				public void onCompleted(GraphUser user, Response response) {
					if (user != null) {
						//Get user info
						uId = user.getId();
					}
				}				
			}).executeAsync();
		}
	}
}
