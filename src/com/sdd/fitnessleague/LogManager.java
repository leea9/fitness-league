/**
 * 
 */
package com.sdd.fitnessleague;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.utils.URLEncodedUtils;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * @author leea9
 *
 */
public class LogManager {
	static InputStream is = null;
	static String json = "";
	
	/**
	 * Method to add the completed exercise to the log table
	 * @param accountID The account ID of the current user
	 * @param exerciseID The current exercise ID
	 * @param weight1 The weight of the first set
	 * @param weight2 The weight of the second set
	 * @param weight3 The weight of the third set
	 * @param rep1 The amount of repetitions in the first set
	 * @param rep2 The amount of repetitions in the second set
	 * @param rep3 The amount of repetitions in the third set
	 * @return 1 - if successful, 0 - otherwise
	 */
	public static int AddCompleteExercise(String accountID, String exerciseID, String weight1, String weight2, String weight3, String rep1, String rep2, String rep3){
		List<NameValuePair> pairs = new ArrayList<NameValuePair>();
		pairs.add(new BasicNameValuePair("accountid", accountID));
		pairs.add(new BasicNameValuePair("exerciseid", exerciseID));
		pairs.add(new BasicNameValuePair("weight1", weight1));
		pairs.add(new BasicNameValuePair("rep1", rep1));
		pairs.add(new BasicNameValuePair("weight2", weight2));
		pairs.add(new BasicNameValuePair("rep2", rep2));
		pairs.add(new BasicNameValuePair("weight3", weight3));
		pairs.add(new BasicNameValuePair("rep3", rep3));
		JSONArray result = makeRequest("http://72.224.16.50/fl/addcomplete.php", "POST", pairs);
		try {
			for(int i = 0; i < result.length();){
				JSONObject jsObj = result.getJSONObject(i);
				return jsObj.getInt("success");
			}
			return 0;
		} catch (JSONException e) {
			e.printStackTrace();
			return 0;
		}
	}
	
	/**
	 * Method to get a completed exercise from the database 
	 *  given an account ID and exercise ID
	 * @param accountID The account ID to look for
	 * @param exerciseID The exercise ID to look for
	 * @return A JSONArray with the results
	 */
	public static JSONArray GetCompletedExercise(String accountID, String exerciseID){
		List<NameValuePair> pair = new ArrayList<NameValuePair>();
		pair.add(new BasicNameValuePair("accountID", accountID));
		pair.add(new BasicNameValuePair("exerciseID", exerciseID));
		JSONArray exercise = makeRequest("http://72.224.16.50/fl/getcomplete.php", "GET", pair);
		return exercise;
	}
	
	/**
	 * Method to get all the logged exercises for a specific user
	 * @param accountID The account ID to search for
	 * @return A JSONArray with the results
	 */
	public static JSONArray GetCompletedExerciseByUser(String accountID){
		List<NameValuePair> pair = new ArrayList<NameValuePair>();
		pair.add(new BasicNameValuePair("accountID", accountID));
		JSONArray exercise = makeRequest("http://72.224.16.50/fl/getcomplete.php", "GET", pair);
		return exercise;
	}
	
	/**
	 * A method to POST and GET from a specified URL
	 * @param url The url to POST or GET from
	 * @param method The method to use (POST or GET)
	 * @param args The variables as basic name/value pairs to POST or GET
	 * @return A JSONArray with the results of the corresponding request
	 */
	public static JSONArray makeRequest(String url, String method, List<NameValuePair> args){
		JSONArray fResult = null;
		try{
			// Try to make the GET request
			if(method == "GET"){
				DefaultHttpClient hClient = new DefaultHttpClient();
				if(args != null){
					String getVar = URLEncodedUtils.format(args, "utf-8");
					url += "?" + getVar;
				}
				HttpGet hGet = new HttpGet(url);
				
				HttpResponse hResponse = hClient.execute(hGet);
				HttpEntity hEntity = hResponse.getEntity();
				is = hEntity.getContent();
			}
			//Try to make POST request
			else if(method == "POST"){
                DefaultHttpClient httpClient = new DefaultHttpClient();
                HttpPost hPost = new HttpPost(url);
                hPost.setEntity(new UrlEncodedFormEntity(args));
 
                HttpResponse hResponse = httpClient.execute(hPost);
                HttpEntity hEntity = hResponse.getEntity();
                is = hEntity.getContent();
			}
			
		}
		catch(UnsupportedEncodingException e){
			e.printStackTrace();
		}
		catch(IOException e){
			e.printStackTrace();
		}
		
		// Try reading the JSON returned
		try{
			BufferedReader reader = new BufferedReader(new InputStreamReader(is, "utf-8"));
			JSONArray res = null;
			while((json = reader.readLine()) != null){
				res = new JSONArray(json);
			}
			is.close();
			fResult = res;
		}
		catch(Exception e){
			e.printStackTrace();
		}
		return fResult;
	}
}
