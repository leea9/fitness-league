package com.sdd.fitnessleague;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.utils.URLEncodedUtils;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class WorkoutManager {
	static InputStream is = null;
	static String json = "";
	
	
	/**
	 * Method to add a workout to the database
	 * @param accountID The account ID of the user
	 * @param name The name of the workout given by the user
	 * @return 1 - if successful, 0 - if failure
	 */
	public static int addWorkout(String accountID, String name){
		List<NameValuePair> pairs = new ArrayList<NameValuePair>();
		pairs.add(new BasicNameValuePair("accountid", accountID));
		pairs.add(new BasicNameValuePair("workoutname", name));
		pairs.add(new BasicNameValuePair("method", "addworkout"));
		JSONArray result = makeRequest("http://72.224.16.50/fl/workout.php", "POST", pairs);
		try {
			for(int i = 0; result != null && i < result.length();){
				JSONObject jsObj = result.getJSONObject(i);
				return jsObj.getInt("success");
			}
			return 0;
		} catch (JSONException e) {
			e.printStackTrace();
			return 0;
		}
	}
	
	/**
	 * Method to add an exercise to a specific workout ID
	 * @param workoutID The workout ID to add exercise to
	 * @param exerciseID The exercise ID to add to workout
	 * @return 1 - if successful, 0 - if failure
	 */
	public static int addExerciseToWorkout(String workoutName, String exerciseID, String accountID){
		List<NameValuePair> pairs = new ArrayList<NameValuePair>();
		pairs.add(new BasicNameValuePair("workoutname", workoutName));
		pairs.add(new BasicNameValuePair("exerciseid", exerciseID));
		pairs.add(new BasicNameValuePair("accountid", accountID));
		pairs.add(new BasicNameValuePair("method", "addexercisetoworkout"));
		JSONArray result = makeRequest("http://72.224.16.50/fl/workout.php", "POST", pairs);
		try {
			for(int i = 0; i < result.length();){
				JSONObject jsObj = result.getJSONObject(i);
				return jsObj.getInt("success");
			}
			return 0;
		} catch (JSONException e) {
			e.printStackTrace();
			return 0;
		}
	}
	
	/**
	 * Method to return a workout given the name of the workout with the corresponding exercise names
	 * @param workoutName The workout name to get
	 * @param accountID The account ID of the user
	 * @return JSONArray with the returned information from the query
	 * 
	 *
	 * SQL Query: SELECT resWorkouts.workoutName, exercises.`name`
	 *				FROM
	 *				(SELECT thisWork.workoutName, workouts.exerciseId
	 *					FROM
	 *						(SELECT user_workout.`Name` as workoutName, user_workout.id as workoutID
	 *						 FROM user_workout
	 *						 WHERE user_workout.`Name` = '$workoutName' AND user_workout.accountId = '$accountID'
	 *						) thisWork
	 *					LEFT JOIN workouts
	 *					ON workouts.userWorkoutId = thisWork.workoutID
	 *				) resWorkouts
	 *				LEFT JOIN exercises
	 *				ON resWorkouts.exerciseId = exercises.id;
	 *	Columns returned: workoutName, exerciseName
	 */
	public static JSONArray getWorkoutByName(String workoutName, String accountID){
		List<NameValuePair> pair = new ArrayList<NameValuePair>();
		pair.add(new BasicNameValuePair("accountid", accountID));
		pair.add(new BasicNameValuePair("workoutname", workoutName));
		pair.add(new BasicNameValuePair("method", "getworkoutbyname"));
		JSONArray exercise = makeRequest("http://72.224.16.50/fl/workout.php", "GET", pair);
		return exercise;
	}
	
	/**
	 * Method to get all workouts by user ID
	 * @param accountID The user ID of the workouts to get
	 * @return JSONArray containing the results of the query below
	 * 
	 * SQL Query: SELECT * FROM user_workout WHERE accountId = '$accountID'"
	 */
	public static JSONArray getAllWorkouts(String accountID){
		List<NameValuePair> pair = new ArrayList<NameValuePair>();
		pair.add(new BasicNameValuePair("accountid", accountID));
		pair.add(new BasicNameValuePair("method", "getallworkouts"));
		JSONArray exercise = makeRequest("http://72.224.16.50/fl/workout.php", "GET", pair);
		return exercise;
	}
	
	/**
	 * A method to POST and GET from a specified URL
	 * @param url The url to POST or GET from
	 * @param method The method to use (POST or GET)
	 * @param args The variables as basic name/value pairs to POST or GET
	 * @return A JSONArray with the results of the corresponding request
	 */
	public static JSONArray makeRequest(String url, String method, List<NameValuePair> args){
		JSONArray fResult = null;
		try{
			// Try to make the GET request
			if(method == "GET"){
				DefaultHttpClient hClient = new DefaultHttpClient();
				if(args != null){
					String getVar = URLEncodedUtils.format(args, "utf-8");
					url += "?" + getVar;
				}
				HttpGet hGet = new HttpGet(url);
				
				HttpResponse hResponse = hClient.execute(hGet);
				HttpEntity hEntity = hResponse.getEntity();
				is = hEntity.getContent();
			}
			//Try to make POST request
			else if(method == "POST"){
                DefaultHttpClient httpClient = new DefaultHttpClient();
                HttpPost hPost = new HttpPost(url);
                hPost.setEntity(new UrlEncodedFormEntity(args));
 
                HttpResponse hResponse = httpClient.execute(hPost);
                HttpEntity hEntity = hResponse.getEntity();
                is = hEntity.getContent();
			}
			
		}
		catch(UnsupportedEncodingException e){
			e.printStackTrace();
		}
		catch(IOException e){
			e.printStackTrace();
		}
		
		// Try reading the JSON returned
		try{
			BufferedReader reader = new BufferedReader(new InputStreamReader(is, "utf-8"));
			JSONArray res = null;
			while((json = reader.readLine()) != null){
				res = new JSONArray(json);
			}
			is.close();
			fResult = res;
		}
		catch(Exception e){
			e.printStackTrace();
		}
		return fResult;
	}
}
