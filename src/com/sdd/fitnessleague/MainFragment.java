package com.sdd.fitnessleague;

import android.support.v4.app.Fragment;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.facebook.Session;
import com.facebook.SessionState;
import com.facebook.UiLifecycleHelper;
import com.facebook.widget.LoginButton;

/**
 * MainFragment is used to handle the Facebook login and implemented in MainActivity 
 * @author Calvin Leung Huang
 */
public class MainFragment extends Fragment{
	
	private static final String TAG = "MainFragment";
	private UiLifecycleHelper uiHelper;
	
	
	/**
	 * Creates a instance of UiLifecycleHelper to be used by the Facebook API
	 */
	@Override
	public void onCreate(Bundle savedInstanceState) {
	    super.onCreate(savedInstanceState);
	    uiHelper = new UiLifecycleHelper(getActivity(), callback);
	    uiHelper.onCreate(savedInstanceState);
	}
	
	/**
	 * Creates the Facebook login button
	 */
	@Override
	public View onCreateView(LayoutInflater inflater, 
	        ViewGroup container, 
	        Bundle savedInstanceState) {
		LinearLayout wrapper = new LinearLayout(getActivity());
	    View view = inflater.inflate(R.layout.activity_login, wrapper, true);

	    LoginButton authButton = (LoginButton) view.findViewById(R.id.authButton);
	    authButton.setFragment(this);
	    
	    return wrapper;
	}
	
	/**
	 * Function that gets called when the state of the session changes, such as user going from logged out
	 * to logged in and vice-versa
	 * @param session Session of the user
	 * @param state State of the session
	 * @param exception Exception that might be caught
	 */
	private void onSessionStateChange(Session session, SessionState state, Exception exception) {
	    if (state.isOpened()) {
	        Log.i(TAG, "Logged in...");
	    } else if (state.isClosed()) {
	        Log.i(TAG, "Logged out...");
	    }
	}
	
	/**
	 * creates a callback status that is used in UiLifecycleHelper
	 */
	private Session.StatusCallback callback = new Session.StatusCallback() {
	    @Override
	    public void call(Session session, SessionState state, Exception exception) {
	        onSessionStateChange(session, state, exception);
	    }
	};
	
	/**
	 * Fucntion that gets called when the application resumes to get the session
	 * of the user back
	 */
	@Override
	public void onResume() {
	    super.onResume();
	    uiHelper.onResume();
	    
	    Session session = Session.getActiveSession();
	    if (session != null &&
	           (session.isOpened() || session.isClosed()) ) {
	        onSessionStateChange(session, session.getState(), null);
	    }

	    uiHelper.onResume();	    
	}

	/**
	 * Function that gets called when an activity has a request
	 */
	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
	    super.onActivityResult(requestCode, resultCode, data);
	    uiHelper.onActivityResult(requestCode, resultCode, data);
	}

	/**
	 * Function that gets called when the application is paused
	 */
	@Override
	public void onPause() {
	    super.onPause();
	    uiHelper.onPause();
	}

	/**
	 * Function that gets called when the application is destroyed
	 */
	@Override
	public void onDestroy() {
	    super.onDestroy();
	    uiHelper.onDestroy();
	}

	/**
	 * Function that gets called when the state of the instance needs
	 * to be stored
	 */
	@Override
	public void onSaveInstanceState(Bundle outState) {
	    super.onSaveInstanceState(outState);
	    uiHelper.onSaveInstanceState(outState);
	}
	
}
