package com.sdd.fitnessleague;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.utils.URLEncodedUtils;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;

/**
 * A class to control database operations between
 * the exercises and the actual database
 * @author Varun Behl
 *
 */
public class ExerciseManager {
	static InputStream is = null;
	static String json = "";
	
	/**
	 * Method to add exercise to a database given a name and a description
	 * @param name The name of the exercise to add
	 * @param desc The description of the exercise to add
	 * @return 0 - if failure, 1 - if successful
	 */
	public static int AddExercise(String name, String desc){
		List<NameValuePair> pairs = new ArrayList<NameValuePair>();
		pairs.add(new BasicNameValuePair("name", name));
		pairs.add(new BasicNameValuePair("description", desc));
		JSONArray result = makeRequest("http://72.224.16.50/fl/addexercise.php", "POST", pairs);
		try {
			for(int i = 0; i < result.length();){
				JSONObject jsObj = result.getJSONObject(i);
				return jsObj.getInt("success");
			}
			return 0;
		} catch (JSONException e) {
			e.printStackTrace();
			return 0;
		}
	}
	
	/**
	 * Method to get an exercise by its name
	 * @param name The name of the exercise to get from the database
	 * @return A JSONArray with the information of the requested exercise
	 */
	public static JSONArray GetExerciseByName(String name){
		List<NameValuePair> pair = new ArrayList<NameValuePair>();
		pair.add(new BasicNameValuePair("name", name));
		JSONArray exercise = makeRequest("http://72.224.16.50/fl/getexercise.php", "GET", pair);
		return exercise;
	}
	
	/**
	 * Method to return all exercises from the database
	 * @return A JSONArray containing all exercises from the database
	 */
	public static JSONArray GetAllExercises(){
		return makeRequest("http://72.224.16.50/fl/getallexercises.php", "GET", null);
	}
	
	/**
	 * A method to POST and GET from a specified URL
	 * @param url The url to POST or GET from
	 * @param method The method to use (POST or GET)
	 * @param args The variables as basic name/value pairs to POST or GET
	 * @return A JSONArray with the results of the corresponding request
	 */
	public static JSONArray makeRequest(String url, String method, List<NameValuePair> args){
		JSONArray fResult = null;
		try{
			// Try to make the GET request
			if(method == "GET"){
				DefaultHttpClient hClient = new DefaultHttpClient();
				if(args != null){
					String getVar = URLEncodedUtils.format(args, "utf-8");
					url += "?" + getVar;
				}
				HttpGet hGet = new HttpGet(url);
				
				HttpResponse hResponse = hClient.execute(hGet);
				HttpEntity hEntity = hResponse.getEntity();
				is = hEntity.getContent();
			}
			//Try to make POST request
			else if(method == "POST"){
                DefaultHttpClient httpClient = new DefaultHttpClient();
                HttpPost hPost = new HttpPost(url);
                hPost.setEntity(new UrlEncodedFormEntity(args));
 
                HttpResponse hResponse = httpClient.execute(hPost);
                HttpEntity hEntity = hResponse.getEntity();
                is = hEntity.getContent();
			}
			
		}
		catch(UnsupportedEncodingException e){
			e.printStackTrace();
		}
		catch(IOException e){
			e.printStackTrace();
		}
		
		// Try reading the JSON returned
		try{
			BufferedReader reader = new BufferedReader(new InputStreamReader(is, "utf-8"));
			JSONArray res = null;
			while((json = reader.readLine()) != null){
				res = new JSONArray(json);
			}
			is.close();
			fResult = res;
		}
		catch(Exception e){
			e.printStackTrace();
		}
		return fResult;
	}
}