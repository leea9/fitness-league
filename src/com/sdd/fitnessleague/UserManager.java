package com.sdd.fitnessleague;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.utils.URLEncodedUtils;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;

/**
 * A class to control database operations between
 * the accounts and the actual database
 * @author Alvin Lee
 */
public class UserManager {
	static InputStream is = null;
	static String json = "";
	
	/**
	 * Function that adds an account to the database
	 * using a web service call through PHP
	 * @param ID The ID of the user to add
	 * @param username The username of the user to add
	 * @param password The password of the new user to add
	 * @return An integer that determines if operation was successful (1) or failure (0)
	 */
	public static int addAccount(String ID, String username, String password){
		List<NameValuePair> pairs = new ArrayList<NameValuePair>();
		pairs.add(new BasicNameValuePair("accountID", ID));
		pairs.add(new BasicNameValuePair("username", username));
		pairs.add(new BasicNameValuePair("password", password));
		JSONArray result = makeRequest("http://72.224.16.50/fl/addaccount.php", "POST", pairs);
		try {
			JSONObject jsObj = result.getJSONObject(0);
			return jsObj.getInt("success");
		} catch (JSONException e) {
			e.printStackTrace();
			return 0;
		}
	}
	
	/**
	 * Function to add a user to the database
	 * @param aID The account ID of the user
	 * @param fName The first name of the user
	 * @param lName The last name of the user
	 * @param birthday The birthday of the user as a date object
	 * @param height The height of the user
	 * @param weight The weight of the user
	 * @param mWidth The muscle width of the user
	 * @return An integer to determine if the operation failed (0) or succeeded (1)
	 */
	public static int AddUser(String aID, String fName, String lName, String birthday, String height, String weight, String mWidth){
		List<NameValuePair> pairs = new ArrayList<NameValuePair>();
		pairs.add(new BasicNameValuePair("accountID", aID));
		pairs.add(new BasicNameValuePair("firstname", fName));
		pairs.add(new BasicNameValuePair("lastname", lName));
		pairs.add(new BasicNameValuePair("birthday", birthday));
		pairs.add(new BasicNameValuePair("height", height));
		pairs.add(new BasicNameValuePair("weight", weight));
		pairs.add(new BasicNameValuePair("musclewidth", mWidth));
		JSONArray result = makeRequest("http://72.224.16.50/fl/adduser.php", "POST", pairs);
		try {
			for(int i = 0; i < result.length();){
				JSONObject jsObj = result.getJSONObject(i);
				return jsObj.getInt("success");
			}
			return 0;
		} catch (JSONException e) {
			e.printStackTrace();
			return 0;
		}
	}
	
	/**
	 * Method to edit a user's details once he is added into the database
	 * @param aID The account ID of the user
	 * @param fName The first name of the user
	 * @param lName The last name of the user
	 * @param birthday The birthday of the user as a date object
	 * @param height The height of the user
	 * @param weight The weight of the user
	 * @param mWidth The muscle width of the user
	 * @return An integer to determine if the operation failed (0) or succeeded (1)
	 */
	public static int EditUser(String aID, String height, String weight, String mWidth){
		List<NameValuePair> pairs = new ArrayList<NameValuePair>();
		pairs.add(new BasicNameValuePair("accountID", aID));
		pairs.add(new BasicNameValuePair("height", height));
		pairs.add(new BasicNameValuePair("weight", weight));
		pairs.add(new BasicNameValuePair("musclewidth", mWidth));
		JSONArray result = makeRequest("http://72.224.16.50/fl/edituser.php", "POST", pairs);
		try {
			JSONObject jsObj = result.getJSONObject(0);
			return jsObj.getInt("success");
		} catch (JSONException e) {
			e.printStackTrace();
			return 0;
		}
	}
	
	/**
	 * Method to get a user given the user's ID
	 * @param id The id of the user
	 * @return A JSONArray with the corresponding user
	 */
	public static JSONArray GetUserByID(String id){
		List<NameValuePair> pair = new ArrayList<NameValuePair>();
		pair.add(new BasicNameValuePair("aid", id));
		JSONArray exercise = makeRequest("http://72.224.16.50/fl/getuser.php", "GET", pair);
		return exercise;
	}
	
	/**
	 * A method to POST and GET from a specified URL
	 * @param url The url to POST or GET from
	 * @param method The method to use (POST or GET)
	 * @param args The variables as basic name/value pairs to POST or GET
	 * @return A JSONArray with the results of the corresponding request
	 */
	public static JSONArray makeRequest(String url, String method, List<NameValuePair> args){
		JSONArray fResult = null;
		try{
			// Try to make the GET request
			if(method == "GET"){
				DefaultHttpClient hClient = new DefaultHttpClient();
				String getVar = URLEncodedUtils.format(args, "utf-8");
				url += "?" + getVar;
				HttpGet hGet = new HttpGet(url);
				
				HttpResponse hResponse = hClient.execute(hGet);
				HttpEntity hEntity = hResponse.getEntity();
				is = hEntity.getContent();
			}
			//Try to make POST request
			else if(method == "POST"){
                DefaultHttpClient httpClient = new DefaultHttpClient();
                HttpPost hPost = new HttpPost(url);
                hPost.setEntity(new UrlEncodedFormEntity(args));
 
                HttpResponse hResponse = httpClient.execute(hPost);
                HttpEntity hEntity = hResponse.getEntity();
                is = hEntity.getContent();
			}
			
		}
		catch(UnsupportedEncodingException e){
			e.printStackTrace();
		}
		catch(IOException e){
			e.printStackTrace();
		}
		
		// Try reading the JSON returned
		try{
			BufferedReader reader = new BufferedReader(new InputStreamReader(is, "utf-8"));
			JSONArray res = null;
			while((json = reader.readLine()) != null){
				res = new JSONArray(json);
			}
			is.close();
			fResult = res;
		}
		catch(Exception e){
			e.printStackTrace();
		}
		return fResult;
	}
}
