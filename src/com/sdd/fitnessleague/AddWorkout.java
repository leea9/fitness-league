package com.sdd.fitnessleague;

import java.util.ArrayList;
import java.util.concurrent.ExecutionException;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.NavUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

/**
 * A class to control the creation of a new workout and add it to the database
 * @author Varun Behl
 */
public class AddWorkout extends BaseActivity {
	MyCustomAdapter dataAdapter = null;
	 
	@Override	 
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_add_workout);
		// Create the adapter to render our data
		//Generate list View from ArrayList
		displayListView();
	}
	 
	 
	 
	private void displayListView(){
    	ArrayList<ExerciseWorkout> exerciseList = new ArrayList<ExerciseWorkout>();
    	ExerciseWorkout temp = new ExerciseWorkout("Temp", false);
		try{
			// Execute command to fetch list of all exercises
	    	ExerciseManagerTask emt = new ExerciseManagerTask();
	    	ArrayList<String> result = emt.execute("listAll").get();
	    	if (result == null){
	    	}
	    	else{
	    		// Add all to the current view
	    		for(int i = 0; i < result.size(); i++){
	    			temp = new ExerciseWorkout(result.get(i), false);
	    			temp.setCode("");
	    			exerciseList.add(temp);
	    		}
		    }
	    }catch (ExecutionException e){
		}catch (InterruptedException e){
		}
	    // Initially select one of the items
		
		//create an ArrayAdaptar from the String Array
		dataAdapter = new MyCustomAdapter(this,
		R.layout.activity_workout_info, exerciseList);
		ListView listView = (ListView) findViewById(R.id.listView1);
		// Assign adapter to ListView
		listView.setAdapter(dataAdapter);
	}
	

	/**
	 * Applies unique menu to the ExerciseListActivity page
	 */
	public boolean onCreateOptionsMenu(Menu menu) {
		super.onCreateOptionsMenu(menu);
		getMenuInflater().inflate(R.menu.workout_button, menu);
		return true;
	}
	
	/**
	 * Implemented to support unique exercise menu
	 */
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case R.id.add_workout_menu:	
			
			EditText editText = (EditText) findViewById(R.id.add_ex_title);
			String title = editText.getText().toString();
			ArrayList<ExerciseWorkout> exerciseList = dataAdapter.exerciseList;
			ArrayList<String> clicked = new ArrayList<String>();
		    for(int i=0;i<exerciseList.size();i++){
		    	ExerciseWorkout exercise = exerciseList.get(i);
		     if(exercise.isSelected()){
		      clicked.add(exercise.getName());
		     }
		    }
			// If a title has not been inputted
			if (title.isEmpty())
			{
	            Toast.makeText(this, "Must enter a title", Toast.LENGTH_SHORT).show();
			}
			else{
				WorkoutManagerTask wmt = new WorkoutManagerTask();
				try { 
					wmt.execute("AddWorkout", uId, title).get();
				} catch (InterruptedException e){
					e.printStackTrace();
				} catch (ExecutionException e) {
			 	 e.printStackTrace();
			 	 }
			 	 ArrayList<String> temp = new ArrayList<String>();
			 	 String eid = "";

			 	 //Go through the clicked exercises and add them to the workout in a loop
			 	 for (int i = 0; i<clicked.size(); i++){
			 		 ExerciseManagerTask emt = new ExerciseManagerTask();
			 		 try {
						temp = emt.execute(clicked.get(i)).get();
						 eid = temp.get(2);
						 wmt = new WorkoutManagerTask();
						 wmt.execute("AddExerciseToWorkout", title, eid,  uId).get();
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					} catch (ExecutionException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
			 	 }
			}			
			NavUtils.navigateUpFromSameTask(this);
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
	//Class to hold arraylist of ExerciseWorkouts
	private class MyCustomAdapter extends ArrayAdapter<ExerciseWorkout> {
		 
		  private ArrayList<ExerciseWorkout> exerciseList;
		 
		  public MyCustomAdapter(Context context, int textViewResourceId, 
		    ArrayList<ExerciseWorkout> exerciseList) {
		   super(context, textViewResourceId, exerciseList);
		   this.exerciseList = new ArrayList<ExerciseWorkout>();
		   this.exerciseList.addAll(exerciseList);
		  }
		 
		  private class ViewHolder {
		   TextView code;
		   CheckBox name;
		  }
		 
		  //Create the view of the page
		  @Override
		  public View getView(int position, View convertView, ViewGroup parent) {
		 
		   ViewHolder holder = null;
		   Log.v("ConvertView", String.valueOf(position));
		 
		   if (convertView == null) {
		   LayoutInflater vi = (LayoutInflater)getSystemService(
		     Context.LAYOUT_INFLATER_SERVICE);
		   convertView = vi.inflate(R.layout.activity_workout_info, null);
		 
		   holder = new ViewHolder();
		   holder.code = (TextView) convertView.findViewById(R.id.code);
		   holder.name = (CheckBox) convertView.findViewById(R.id.checkBox1);
		   convertView.setTag(holder);
		 
		    holder.name.setOnClickListener( new View.OnClickListener() {  
		    	
		     public void onClick(View v) {  
		      CheckBox cb = (CheckBox) v ;  
		      ExerciseWorkout exercise = (ExerciseWorkout) cb.getTag();  
		      exercise.setSelected(cb.isChecked());
		     }  
		    });  
		   } 
		   else {
		    holder = (ViewHolder) convertView.getTag();
		   }
		   
		   ExerciseWorkout exercise = exerciseList.get(position);
		   holder.code.setText(" (" +  exercise.getCode() + ")");
		   holder.name.setText(exercise.getName());
		   holder.name.setChecked(exercise.isSelected());
		   holder.name.setTag(exercise);
		 
		   return convertView;
		 
		  }
		 
		 }		 
		}