/**
 * 
 */
package com.sdd.fitnessleague;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.utils.URLEncodedUtils;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * @author Alvin
 *
 */
public class LevelManager {
	static InputStream is = null;
	static String json = "";
	
	/**
	 * Method to add a new user to the experience table
	 * @param userID The user ID to add to the table
	 * @return 1 - if successful, 0 - if failure
	 */
	public static int addNewUser(String userID){
		List<NameValuePair> pairs = new ArrayList<NameValuePair>();
		pairs.add(new BasicNameValuePair("accountID", userID));
		pairs.add(new BasicNameValuePair("method", "addlvl"));
		JSONArray result = makeRequest("http://72.224.16.50/fl/levels.php", "POST", pairs);
		try {
			JSONObject jsObj = result.getJSONObject(0);
			return jsObj.getInt("success");
		} catch (JSONException e) {
			e.printStackTrace();
			return 0;
		}
	}
	
	/**
	 * Method to set the exp of the user
	 * @param userID The userID of the user to set
	 * @param exp The exp to set
	 * @return 1 - if successful, 0 - if failure
	 */
	public static int setExp(String userID, String exp){
		List<NameValuePair> pairs = new ArrayList<NameValuePair>();
		pairs.add(new BasicNameValuePair("accountID", userID));
		pairs.add(new BasicNameValuePair("exp", exp));
		pairs.add(new BasicNameValuePair("method", "setexp"));
		JSONArray result = makeRequest("http://72.224.16.50/fl/levels.php", "POST", pairs);
		try {
			JSONObject jsObj = result.getJSONObject(0);
			return jsObj.getInt("success");
		} catch (JSONException e) {
			e.printStackTrace();
			return 0;
		}
	}

	/**
	 * Method to set the last activity for a user
	 * @param userID The user id to set the last activity for
	 * @return 1 - if successful, 0 - if failure
	 */
	public static int setLastActivity(String userID){
		List<NameValuePair> pairs = new ArrayList<NameValuePair>();
		pairs.add(new BasicNameValuePair("accountID", userID));
		pairs.add(new BasicNameValuePair("method", "setlastact"));
		JSONArray result = makeRequest("http://72.224.16.50/fl/levels.php", "POST", pairs);
		try {
			JSONObject jsObj = result.getJSONObject(0);
			return jsObj.getInt("success");
		} catch (JSONException e) {
			e.printStackTrace();
			return 0;
		}
	}
	
	/**
	 * Method to get a user's exp given a user ID
	 * @param userID The user ID to set
	 * @return A JSONArray containing the returned query
	 */
	public static JSONArray getExp(String userID){
		List<NameValuePair> pair = new ArrayList<NameValuePair>();
		pair.add(new BasicNameValuePair("accountID", userID));
		pair.add(new BasicNameValuePair("method", "getExp"));
		JSONArray exercise = makeRequest("http://72.224.16.50/fl/levels.php", "GET", pair);
		return exercise;
	}
	
	/**
	 * Method to get the last activity for a given user ID
	 * @param userID The user id to get the last activity for
	 * @return JSONArray containing the returned query
	 */
	public static JSONArray getLastActivity(String userID){
		List<NameValuePair> pair = new ArrayList<NameValuePair>();
		pair.add(new BasicNameValuePair("accountID", userID));
		pair.add(new BasicNameValuePair("method", "getlastact"));
		JSONArray exercise = makeRequest("http://72.224.16.50/fl/levels.php", "GET", pair);
		return exercise;
	}
	
	/**
	 * Method to get a level given a user ID
	 * @param userID The user ID to get
	 * @return 1 - if successful, 0 - if failure
	 */
	public static JSONArray getLevel(String userID){
		List<NameValuePair> pairs = new ArrayList<NameValuePair>();
		pairs.add(new BasicNameValuePair("accountID", userID));
		pairs.add(new BasicNameValuePair("method", "getlvl"));
		
		JSONArray result = makeRequest("http://72.224.16.50/fl/levels.php", "GET", pairs);
		return result;
		
	}
	/**
	 * Method to set the level of a user
	 * @param userID The user ID
	 * @param level The level to set
	 * @return 1 - if successful, 0 - if failure
	 */
	public static int setLevel(String userID, String level){
		List<NameValuePair> pairs = new ArrayList<NameValuePair>();
		pairs.add(new BasicNameValuePair("accountID", userID));
		pairs.add(new BasicNameValuePair("level", level));
		pairs.add(new BasicNameValuePair("method", "setlvl"));
		JSONArray result = makeRequest("http://72.224.16.50/fl/levels.php", "POST", pairs);
		try {
			JSONObject jsObj = result.getJSONObject(0);
			return jsObj.getInt("success");
		} catch (JSONException e) {
			e.printStackTrace();
			return 0;
		}
		
	}
	
	/**
	 * Method to get an achievement given a category and requisite
	 * @param category The category of the achievement
	 * @param requisite The requisite of the achievement
	 * @return 1 - if successful, 0 - if failure
	 */
	public static int getAchievement(String category, String requisite){
		List<NameValuePair> pairs = new ArrayList<NameValuePair>();
		pairs.add(new BasicNameValuePair("category", category));
		pairs.add(new BasicNameValuePair("requisite", requisite));
		pairs.add(new BasicNameValuePair("method", "getachievement"));
		
		JSONArray result = makeRequest("http://72.224.16.50/fl/levels.php", "GET", pairs);
		try {
			JSONObject jsObj = result.getJSONObject(0);
			return jsObj.getInt("success");
		} catch (JSONException e) {
			e.printStackTrace();
			return 0;
		}
	}
	
	/**
	 * A method to POST and GET from a specified URL
	 * @param url The url to POST or GET from
	 * @param method The method to use (POST or GET)
	 * @param args The variables as basic name/value pairs to POST or GET
	 * @return A JSONArray with the results of the corresponding request
	 */
	public static JSONArray makeRequest(String url, String method, List<NameValuePair> args){
		JSONArray fResult = null;
		try{
			// Try to make the GET request
			if(method == "GET"){
				DefaultHttpClient hClient = new DefaultHttpClient();
				if(args != null){
					String getVar = URLEncodedUtils.format(args, "utf-8");
					url += "?" + getVar;
				}
				HttpGet hGet = new HttpGet(url);
				
				HttpResponse hResponse = hClient.execute(hGet);
				HttpEntity hEntity = hResponse.getEntity();
				is = hEntity.getContent();
			}
			//Try to make POST request
			else if(method == "POST"){
                DefaultHttpClient httpClient = new DefaultHttpClient();
                HttpPost hPost = new HttpPost(url);
                hPost.setEntity(new UrlEncodedFormEntity(args));
 
                HttpResponse hResponse = httpClient.execute(hPost);
                HttpEntity hEntity = hResponse.getEntity();
                is = hEntity.getContent();
			}
			
		}
		catch(UnsupportedEncodingException e){
			e.printStackTrace();
		}
		catch(IOException e){
			e.printStackTrace();
		}
		
		// Try reading the JSON returned
		try{
			BufferedReader reader = new BufferedReader(new InputStreamReader(is, "utf-8"));
			JSONArray res = null;
			while((json = reader.readLine()) != null){
				res = new JSONArray(json);
			}
			is.close();
			fResult = res;
		}
		catch(Exception e){
			e.printStackTrace();
		}
		return fResult;
	}
}
