package com.sdd.fitnessleague;

import android.os.AsyncTask;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Asynchronous task for handling user profile to the database using UserManager
 * @author leungc5
 */
public class UserManagerTask extends AsyncTask<String, Void, ArrayList<String>>{

	@Override
	protected ArrayList<String> doInBackground(String... params) {
		ArrayList<String> requestedStrings = new ArrayList<String>();
		try {
			if (params[0].equals("GetProfile")) {
				JSONArray jsonList = UserManager.GetUserByID(params[1]);
				if (jsonList == null || jsonList.length() == 0)
					return null;
				
				for (int i = 0; jsonList != null && i < jsonList.length(); i++) {
					JSONObject jsonObj = jsonList.getJSONObject(i);
					if (jsonObj.has("height"))
						requestedStrings.add(jsonObj.getString("height"));
					if (jsonObj.has("weight"))
						requestedStrings.add(jsonObj.getString("weight"));
					if (jsonObj.has("musclewidth"))
						requestedStrings.add(jsonObj.getString("musclewidth"));
				}
				return requestedStrings;
			}
			else if (params[0].equals("EditProfile")) {
				UserManager.EditUser(params[1], params[2], params[3], params[4]);
				return null;
			}
			else { //CreateProfile
				UserManager.AddUser(params[1], params[2], params[3], params[4], params[5], params[6], params[7]);
				return null;
			}
		}
		catch (Exception e) {
        	e.printStackTrace();
            return null;
		}
	}
}
