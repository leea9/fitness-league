package com.sdd.fitnessleague;

import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;
import android.support.v4.app.NavUtils;

/**
 * This class adds a new exercise to the exercise database.
 * @author willis4
 *
 */
public class AddExercise extends BaseActivity {

	/**
	 * Method to set the visual aspects of the AddExercise activity
	 */
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_add_exercise);
		setupActionBar();
	}
	
	/**
	 * Method is overridden to show the title
	 */
	protected void setupActionBar() {
		super.setupActionBar();
		getActionBar().setDisplayShowTitleEnabled(true);
	}
	
	/**
	 * Method to save the information that was input to the 
	 * fields in the database.
	 * @param view 
	 */
	public void saveNewExercise(View view)
	{
		EditText editText = (EditText) findViewById(R.id.add_ex_title);
		String title = editText.getText().toString();
		
		// If a title has not been inputted
		if (title.isEmpty())
		{
            Toast.makeText(this, "Must enter a title", Toast.LENGTH_SHORT).show();
            return;
		}
	
		editText = (EditText) findViewById(R.id.add_ex_desc);
		String description = editText.getText().toString();
		
		// Execute the command to add the task to the database
		ExerciseManagerTask emt = new ExerciseManagerTask();
		emt.execute("AddExercise", title, description);
		
		NavUtils.navigateUpFromSameTask(this);
	}

}
