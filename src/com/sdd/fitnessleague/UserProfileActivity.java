package com.sdd.fitnessleague;

import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.facebook.Request;
import com.facebook.Response;
import com.facebook.Session;
import com.facebook.model.GraphUser;

import java.util.ArrayList;

/**
 * 
 * @author Calvin Leung Huang
 * 
 */
public class UserProfileActivity extends BaseActivity {

	boolean REGISTERED_ACCOUNT = false;
	String USERID = "";
	String FNAME = "";
	String LNAME = "";
	String BIRTHDAY = "2013-10-23";
	
	/**
	 * onCreate runs once, when the activity is created
	 */
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_userprofile);
		
		Session session = Session.getActiveSession();
		if (session != null && session.isOpened()) {
			Request.newMeRequest(session, new Request.GraphUserCallback() {

				@Override
				public void onCompleted(GraphUser user, Response response) {
					if (user != null) {
						TextView firstName = (TextView) findViewById(R.id.profile_first_name);
						firstName.setText(user.getFirstName());
						
						//Get user info
						USERID = user.getId();
						FNAME = user.getFirstName();
						LNAME = user.getLastName();
						
						
						try{
							//Gets user profile, does not post any data
							UserManagerTask umt = new UserManagerTask();
							ArrayList<String> result = umt.execute("GetProfile", user.getId()).get();
							if (result.size() != 0 || result != null){
								EditText height = (EditText) findViewById(R.id.height);
								EditText weight = (EditText) findViewById(R.id.weight);
								EditText muscle_width = (EditText) findViewById(R.id.muscle_width);
								
								height.setText(result.get(0));
								weight.setText(result.get(1));
								muscle_width.setText(result.get(2));
								
								REGISTERED_ACCOUNT = true;
							}
							
							//Gets user level and experience
							int currentExp = 0;
							int currentLevel = 0;
							
							LevelManagerTask lmt = new LevelManagerTask();
							result = lmt.execute("getExp", user.getId()).get();
							if (result.size() != 0 || result != null){
								currentExp = Integer.parseInt(result.get(0));								
							}
							
							LevelManagerTask lmt2 = new LevelManagerTask();
							result = lmt2.execute("getLevel", user.getId()).get();
							if (result.size() != 0 || result != null){
								currentLevel = Integer.parseInt(result.get(0));								
							}
							
							int nextLevelExp = (int)(10 * power(currentLevel));
							
							//Sets the level and experience text
							TextView level = (TextView) findViewById(R.id.level_in);
							TextView experience = (TextView) findViewById(R.id.experience_in);
														
							level.setText(Integer.toString(currentLevel));
							experience.setText(Integer.toString(currentExp) + "/" + Integer.toString(nextLevelExp));												
						}
						catch (Exception e) {
							e.printStackTrace();
						}
					}
					else
					{
						TextView firstName = (TextView) findViewById(R.id.profile_first_name);
						firstName.setText("No user found!");
					}
				}				
			}).executeAsync();
		}		
	}
	
	/**
	 * Function to calculate the power with base of 1.10
	 * @param exp
	 * @return double 1.10^exp
	 */
	private double power(int exp) {
		if (exp >= 1)
			return 1.10 * (power(exp - 1));
		else
			return 1;
	}

	protected void setupActionBar() {
		super.setupActionBar();
		getActionBar().setDisplayShowTitleEnabled(true);
		getActionBar().setTitle("User Profile");
	}
	
	public void saveProfile(View view) {
		EditText height = (EditText) findViewById(R.id.height);
		EditText weight = (EditText) findViewById(R.id.weight);
		EditText muscle_width = (EditText) findViewById(R.id.muscle_width);
		
		if(REGISTERED_ACCOUNT) {
			try{
				String[] input = new String[]{
						"EditProfile",
						USERID,
						height.getText().toString(),
						weight.getText().toString(),
						muscle_width.getText().toString()
				};
				
				UserManagerTask umt = new UserManagerTask();
				umt.execute(input);
			}
			catch (Exception e) {
				e.printStackTrace();
			}
		} //if user is not registered
		else {
			try{
				String[] input = new String[]{
						"AddProfile",
						USERID,
						FNAME,
						LNAME,
						BIRTHDAY,
						height.getText().toString(),
						weight.getText().toString(),
						muscle_width.getText().toString()
				};
				
				UserManagerTask umt = new UserManagerTask();
				umt.execute(input).get();
			}
			catch (Exception e) {
				e.printStackTrace();
			}
		}
		//Destroy ProfileActivity after user saves profile
		finish();
	}

}
