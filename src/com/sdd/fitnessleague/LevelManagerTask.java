package com.sdd.fitnessleague;

import android.os.AsyncTask;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Asynchronous task for handling level and experience to the database using LevelManager
 * @author Calvin Leung Huang
 */
public class LevelManagerTask extends AsyncTask<String, Void, ArrayList<String>>{

	@Override
	protected ArrayList<String> doInBackground(String... params) {
		ArrayList<String> requestedStrings = new ArrayList<String>();
		try {
			if (params[0].equals("addNewUser")){
				LevelManager.addNewUser(params[1]);
				return null;
			}			
			else if (params[0].equals("setExp")) {
				LevelManager.setExp(params[1], params[2]);
				return null;
			}
			else if (params[0].equals("getExp")) {
				JSONArray jsonList = LevelManager.getExp(params[1]);
				if (jsonList == null || jsonList.length() == 0)
					return null;
				
				for (int i = 0; jsonList != null && i < jsonList.length(); i++) {
					JSONObject jsonObj = jsonList.getJSONObject(i);
					if (jsonObj.has("currentExp"))
						requestedStrings.add(jsonObj.getString("currentExp"));
				}
				return requestedStrings;
			}
			else if (params[0].equals("setLevel")) {
				LevelManager.setLevel(params[1], params[2]);
				return null;
			}
			else if (params[0].equals("getLevel")) {
				JSONArray jsonList = LevelManager.getLevel(params[1]);
				if (jsonList == null || jsonList.length() == 0)
					return null;
				
				for (int i = 0; jsonList != null && i < jsonList.length(); i++) {
					JSONObject jsonObj = jsonList.getJSONObject(i);
					if (jsonObj.has("level"))
						requestedStrings.add(jsonObj.getString("level"));
				}
				return requestedStrings;
			}
			else if (params[0].equals("getAchievement")) {
				int result = LevelManager.getAchievement(params[1], params[2]);
				requestedStrings.add(Integer.toString(result));
				return requestedStrings;
			}
			else { //Get user last activity
				JSONArray jsonList = LevelManager.getLastActivity(params[1]);
				if (jsonList == null || jsonList.length() == 0)
					return null;
				for (int i = 0; jsonList != null && i < jsonList.length(); i++) {
					JSONObject jsonObj = jsonList.getJSONObject(i);
					if (jsonObj.has("success"))
						requestedStrings.add(jsonObj.getString("success"));
				}
				return requestedStrings;
			}
		}
		catch (Exception e) {
        	e.printStackTrace();
            return null;
		}
	}

}
