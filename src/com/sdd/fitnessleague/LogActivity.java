package com.sdd.fitnessleague;

import android.os.Bundle;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;


/**
 * Class to view the data stored within the log
 * @author willis4
 *
 */
public class LogActivity extends BaseActivity {

	/**
	 * Collects all logs and sets the view
	 */
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_log);
		
		getAllLogs();
	}
	
	/**
	 * Sets the title as displayed
	 */
	protected void setupActionBar() {
		super.setupActionBar();
		getActionBar().setDisplayShowTitleEnabled(true);
	}
	
	/**
	 * Fetches all logs for the user from the database
	 */
	public void getAllLogs()
	{		
		ArrayList<String> logList = new ArrayList<String>();
		try{
			ArrayList<String> rv;
			LogManagerTask lmt = new LogManagerTask();
			 rv = lmt.execute("GetCompletedExerciseByUser", uId).get();
			 if (rv != null && rv.size() > 0)
				 logList.addAll(rv);			
		}
		catch (Exception e) {
			e.printStackTrace();
			Toast.makeText(this, e.toString(), Toast.LENGTH_LONG).show();
		}
		
		// Add logs found to the current view
		final ListView listview = (ListView) findViewById(R.id.log_view);
		ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, logList);
	    listview.setAdapter(adapter);  
	}
	
}
